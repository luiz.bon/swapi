﻿using DataAccess;
using Microsoft.AspNetCore.Mvc;
using people.api.Models;

namespace people.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase
    {
        private readonly Repository<People> _repository;

        public PeopleController(Repository<People> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public IActionResult Get(int? page)
        {
            var results = _repository.GetList(page);

            if (results.Results.Count > 0)
                return Ok(results);

            return NotFound();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var result = _repository.GetById(id);
            if (result == null)
                return NotFound();
            return Ok(_repository.GetById(id));
        }
    }
}
