﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.AspNetCore.Mvc.Testing;
using Newtonsoft.Json;
using people.api.Models;
using Xunit;

namespace api.tests.Controllers
{
    public class PeopleApiTests: IClassFixture<ApiWebApplicationFactory<people.api.Startup>>
    {
        private readonly HttpClient _client;

        public PeopleApiTests(ApiWebApplicationFactory<people.api.Startup> factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
        }

        [Fact]
        public async Task Get_People_ReturnsListOfPeople()
        {
            var result = await _client.GetAsync("/api/people");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            var content = JsonConvert.DeserializeObject<PagedList<People>>(await result.Content.ReadAsStringAsync());
            Assert.True(content.TotalCount > 0);
            Assert.Equal(1, content.Page);
        }

        [Theory]
        [InlineData(2)]
        public async Task Get_PagedPeople_ReturnsListOfPeople(int page)
        {
            var result = await _client.GetAsync($"/api/people?page={page}");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            var content = JsonConvert.DeserializeObject<PagedList<People>>(await result.Content.ReadAsStringAsync());
            Assert.True(content.TotalCount > 0);
            Assert.Equal(page, content.Page);
        }

        [Fact]
        public async Task Get_NonExistingPagePeople_Returns404()
        {
            var result = await _client.GetAsync("/api/people?page=10");

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }

        [Theory]
        [InlineData(1)]
        public async Task Get_People_ReturnsCorrectId(int id)
        {
            var result = await _client.GetAsync($"/api/people/{id}/");

            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            var content = JsonConvert.DeserializeObject<People>(await result.Content.ReadAsStringAsync());
            Assert.Equal(id, content.Id);
        }

        [Theory]
        [InlineData(-1)]
        public async Task Get_NonExistingPeople_Returns404(int id)
        {
            var result = await _client.GetAsync($"/api/people/{id}");

            Assert.Equal(HttpStatusCode.NotFound, result.StatusCode);
        }
    }
}
