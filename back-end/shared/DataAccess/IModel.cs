﻿namespace DataAccess
{
    public interface IModel
    {
        int Id { get; set; }
    }
}