﻿using System.Collections.Generic;

namespace DataAccess
{
    public class PagedList<T>
    {
        public int Page { get; set; }
        public bool HasNext { get; set; }
        public bool HasPrevious { get; set; }
        public int TotalCount { get; set; }
        public IList<T> Results { get; set; }
    }
}