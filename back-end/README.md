## Back-end task

### `Implementation`

The current implementation uses a microservice approach. As it's a Proof of Concept it only implements two services, People and Films, the database is one json file on each service.

The solution is configured to use an API Gateway which orchestrate the calls between the underlying services, control the access quota, implements cache and Quality of Service.

The follwing diagram shows how the solution is architected.

![Diagram](/back-end/diagram.png)

Since there's no data manipulation on this system, I left the messaging system out of the solution.

### `Tests`

I've implemented integration tests based on the ASP.NET Core testing patterns.

### `Monolith approach`

For such a small project is possible to achieve the desired capacity with a simple container hosted on an orchestration system, Kubernetes or similar, which provide the horizontal scalability needed for a high demand system with the database hosted on a cloud provider which provides scalability, Aurora or DynamoDB are options.

### `Microservices approach`

For the sake of this exercise is possible to create a Microservices architecture.
Following the documentation is possible to define each domain easily:

-   People
-   Films
-   Starships
-   Vehicles
-   Species
-   Planets

Where each service is composed of an API with its own database.

As each domain has a reference the other, ex. People participate in Films, a communication layer between the services is needed.
This communication is done via a Messaging System where data related changes are published to the system, and the corresponding subscribers read and apply the changes on their local databases.
There is a gap between the message published, and the subscriber read the data, this is a known behaviour and is called eventual consistency.

As mentioned on the `monolith` approach, the container approach is still valid for this case, where each service is served as a container, and the container orchestrator scale only the services needed.
Another benefit of this approach is in case one of the services going down, ex. A database outage can prevent all container instances from working, the rest of the system still works, so only the users who use the specific service are affected.

As the system utilises navigation properties, ex. The films a person participate is a list of URLs to the film, is a good idea to have an API Gateway layer which is a central point where the clients receive the requests and orchestrate the calls to the services underneath.
This layer can also transform the responses.

At this gateway is possible to manage the security in a central place where the authentication is done in one central place. As this layer is responsible for the authentication, it's easy to implement user quotas.

---

On both approaches is essential to implement some cross-cutting concern features:

-   Logging: to track the requests and identify issues (ES Kibana/Raygun)
-   Monitoring: to monitor the health of the system (AppInsights/New Relic)
-   Security: to prevent the system from external attacks (WAF)
