import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Container, Row, Col } from 'reactstrap';
import { Table, Pagination } from '../components';
import Details from './Details';
import { requestPeople } from '../actions';

export class Main extends Component {
  constructor(props) {
    super(props);
    this.selectPerson = this.selectPerson.bind(this);
    this.fetchPeople = this.fetchPeople.bind(this);
    this.state = {};
  }

  componentDidMount() {
    const { next } = this.props;
    this.fetchPeople(next);
  }

  selectPerson(item) {
    this.setState({
      selectedPerson: item
    });
  }

  fetchPeople(url) {
    const { dispatch } = this.props;
    this.selectPerson(undefined);
    dispatch(requestPeople(url));
  }

  render() {
    const { isFetching, results } = this.props;
    return (
      <Container>
        {isFetching && 'Loading...'}
        {!isFetching && results && (
          <>
            <Row>
              <Col>
                <Table people={results} selectPerson={this.selectPerson} />
              </Col>
            </Row>
            <Row>
              <Col>
                <Pagination
                  previous={this.props.previous}
                  next={this.props.next}
                  onClick={this.fetchPeople}
                />
              </Col>
            </Row>
          </>
        )}
        {this.state.selectedPerson && (
          <Details
            {...this.state.selectedPerson}
            onClose={() => this.selectPerson(undefined)}
          />
        )}
      </Container>
    );
  }
}

Main.propTypes = {
  isFetching: PropTypes.bool,
  next: PropTypes.string,
  previous: PropTypes.string,
  results: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      height: PropTypes.string,
      mass: PropTypes.string
    })
  ),
  dispatch: PropTypes.func
};

const mapStateToProps = state => {
  return state.people;
};

export default connect(mapStateToProps)(Main);
