import { RECEIVE_FILMS, REQUEST_FILMS } from '../../actions/films';
import films from '../films';

describe('Films reducer', () => {
  test('When request it should set isFetching to true', () => {
    const state = films(undefined, {
      type: REQUEST_FILMS
    });

    expect(state.isFetching).toBe(true);
  });

  test('When receive it shuould map results properly', () => {
    const response = [{ title: 'The Empire Strikes Back' }];
    const state = films(
      {
        isFetching: true
      },
      {
        type: RECEIVE_FILMS,
        result: response
      }
    );

    expect(state.isFetching).toBe(false);
    expect(state.results[0]).toEqual(response[0].title);
  });
});
