import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducers from './reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = [thunkMiddleware];

if (process.env.NODE_ENV === `development`) {
  middlewares.push(createLogger());
}

const enhancer = composeEnhancers(applyMiddleware(...middlewares));

export default preloadedState => {
  return createStore(reducers, preloadedState, enhancer);
};
