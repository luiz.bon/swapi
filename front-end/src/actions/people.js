export const REQUEST_PEOPLE = 'REQUEST_PEOPLE';
export const RECEIVE_PEOPLE = 'RECEIVE_PEOPLE';

const requestPeople = () => {
  return {
    type: REQUEST_PEOPLE
  };
};

const receivePeople = result => {
  return {
    type: RECEIVE_PEOPLE,
    result
  };
};

const fetchPeople = url => {
  const endpoint = url || 'https://swapi.co/api/people/';

  return async dispatch => {
    dispatch(requestPeople());
    const resp = await fetch(endpoint);
    const json = await resp.json();
    return dispatch(receivePeople(json));
  };
};

export default url => {
  return dispatch => {
    return dispatch(fetchPeople(url));
  };
};
