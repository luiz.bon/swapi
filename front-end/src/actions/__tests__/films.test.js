import films, { RECEIVE_FILMS, REQUEST_FILMS } from '../films';
import fetchMock from 'fetch-mock';

const dispatch = jest.fn(action => action);

describe('films action creator', () => {
  beforeEach(() => {
    dispatch.mockClear();
    fetchMock.restore();
  });

  test('on action call it should dispatch request and receive', async () => {
    const mockedData1 = { mock: 'data1' };
    const mockedData2 = { mock: 'data2' };
    fetchMock.getOnce('http://swapi/1', mockedData1);
    fetchMock.getOnce('http://swapi/2', mockedData2);

    await films(['http://swapi/1', 'http://swapi/2'])(dispatch)(dispatch);

    expect(dispatch).toHaveBeenCalledWith({
      type: REQUEST_FILMS
    });
    expect(dispatch).toHaveBeenLastCalledWith({
      type: RECEIVE_FILMS,
      result: [mockedData1, mockedData2]
    });
  });
});
