import requestPeople from './people';
import requestFilms from './films';

export { requestPeople, requestFilms };
