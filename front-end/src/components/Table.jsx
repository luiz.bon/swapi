import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table as BootstrapTable } from 'reactstrap';

class Table extends Component {
  render() {
    const { people, selectPerson } = this.props;
    return (
      <BootstrapTable hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Height</th>
            <th>Mass</th>
          </tr>
        </thead>
        <tbody>
          {people.map(item => {
            return (
              <tr key={item.name} onClick={() => selectPerson(item)}>
                <td>{item.name}</td>
                <td>{item.height}</td>
                <td>{item.mass}</td>
              </tr>
            );
          })}
        </tbody>
      </BootstrapTable>
    );
  }
}

Table.propTypes = {
  people: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      height: PropTypes.string,
      mass: PropTypes.string
    })
  ),
  selectPerson: PropTypes.func
};

export default Table;
